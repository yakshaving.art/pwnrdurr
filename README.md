# pwnrdurr

The Durr that ensures that your code can't get pwned

## How to use

1. You setup a .pwnrdurr.yaml file in your repo (using variables to prevent
   people from changing it).
2. Add a PWNRDURR_GITLAB_TOKEN secret for your pipeline.
3. Add a pwnrdurr CI stage
4. PROFIT: on every MR execution there will be a pipeline that will fail if
   pwnrdurr conditions are not set.

## Configuration sample

```yaml
- name: the name of your rule
  path: a_glob_pattern
  owners:
  - gitlab.username
  required: all
```

## What will happen

When your stage executes it will check which files are being changed in the current MR.

If the files match any of the given glob, it will pull the emoji reactions using the
award API to check that we have as many approvals from the owners list as
required are configured.

If the conditions are met, allest ist good.

If not, it will fail with a descriptive message with what happened to allow
the MR creator get the right approvals.

## Possible future rules

- `required: all` would only approve when all the owners grant a :+1:
- `required: any` would approve when only one owner grants a :+1:
